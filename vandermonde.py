#!/usr/bin/env python3
'''Matrice carrée de Vandermonde, deux implémentations.'''

import numpy as np

def vdm1(vector, dtype=np.float64):
    n = len(vector)
    M = np.zeros((n,n), dtype=dtype)
    for i in range(n):
        for j in range(n):
            M[i][j] = vector[i]**j
    return M

def vdm2(vector, dtype=np.float64):
    return np.array(
            [ [ xi**j for j in range(len(vector)) ] for xi in vector ], dtype=dtype)

def detvdm(vector):
    n = len(vector)
    det = 1
    for i in range(n):
        for j in range(i+1, n):
            det *= vector[j] - vector[i]
    return det

# TODO: faire devdm avec compréhension

vector = np.array(range(2,7))
print(vector)
print(vdm1( vector ))
print(vdm2( vector ))
print(detvdm( vector ), np.linalg.det(vdm2( vector)))

for _ in range(10):
    vector = np.random.random(10)
    print(x:=detvdm( vector ), '\t', y:=np.linalg.det(vdm2( vector)), abs(x-y))


#!/usr/bin/env python3
'''Passe commande du petit déjeuner (version avec une liste)'''

order = input('Commande : ').split()

if 'spam' in order:
    order += [ 'good!' ]
elif 'ham' in order:
    order += [ 'spam' ]
else:
    order += [ 'spam' ] * 2

if 'coffee' in order:
    order += [ 'spam' ] * 10

nspam = order.count('spam')

print("**** Fawlty Towers Hotel ****")
print('Assiette :', '\n'.join(order))
print('Spam     :', nspam)
print("**** Service not included ****")


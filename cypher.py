#!/usr/bin/env python3

from string import ascii_lowercase

clear = input('Votre message (en minuscule) : ')
code = int(input('Code secret [1-25] : '))

cypher = []
for c in clear:
    if c in ascii_lowercase:
        cypher.append( chr((ord(c) - ord('a') + code ) % 26 + ord('a')) )
    else:
        cypher.append(c)

print( ''.join(cypher))

print(
        ''.join(
             chr((ord(c) - ord('a') + code ) % 26 + ord('a')) \
                    if c in ascii_lowercase else c \
                    for c in clear  )
        )

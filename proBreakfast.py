#!/usr/bin/env python3
'''Module pour gérer le menu du petit déjeuner'''

import os

with open('prices.txt') as price_file:
    pricedb = {}
    for line in price_file:
        prod, price = line.rstrip().split()
        price = float(price)
        pricedb[prod] = price

def product_price(food):
    if not pricedb:
        print('Warning: dictionnaire des produit vide!')
        print('Warning: vérifiez le fichier prices.txt')
        return None
    # C'est bon on a la base pricedb
    price = pricedb.get(food)
    if price is None:
        print('Warning: produit absent de la base ({}) !'.format(food))
    return price

def breakfast_price(foods):
    total = 0
    for food in foods:
        price = product_price(food)
        total += price if price is not None else 0
    return total

#!/usr/bin/env python3

class City:
    def __init__(self, name, pop=None, area=None, gent=None):
        self.name = name
        self.pop = pop
        self.area = area
        self.gent = gent
    def show(self):
        print(f'Ville de {city.name}')
        # TODO: affichier les autres attributs
    def density(self):
        if self.pop is not None and self.area is not None:
            return self.pop/self.area
        else:
            return None
    def __str__(self):
        return f"Ville de {self.name} ( ... autres attribut ...)"
    def __repr__(self):
        return f'City("{self.name}", ...'

class Capital(City):
    def __init__(self, name, country, pop=None, area=None, gent=None):
        super().__init__(name, pop, area, gent)
        self.country = country
    def show(self):
        super().show()
        print('Capitale de', self.country)

montaigu = City('Montaigu', 20424, 117.9, 'Montacutain')
morlaix  = City('Morlaix')
paris    = Capital('Paris', 'France', 2e6)

for city in [ montaigu, paris, morlaix ]:
    print(f"Densité de {city.name} : {city.density()}") 
    city.show()
    print(city)



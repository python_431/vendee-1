#!/usr/bin/env python3
# pour avoir tkinter sous GNU/Linux (Ubuntu ou Debian)
# sudo apt install python3-tk

import tkinter as tk
# from tkinter import * 

mainwin = tk.Tk()

title = tk.Label(mainwin, text="Liste des villes")
info  = tk.Label(mainwin, text="")

myCities = { 'Paris':'Parisien' , 
             'Bordeaux':'Bordelais',
             'Brest':'Brestois',
             'Lyon':'Lyonnais' } 

citylist = tk.Listbox(mainwin)
for i,city in enumerate(myCities,1):
    citylist.insert(i, city)

def cityselect():
    #print(citylist.get(tk.ACTIVE))
    city = citylist.get(tk.ACTIVE)
    infotext = myCities[city]
    info.config(text=infotext)

validbutton = tk.Button(mainwin,text="Infos",command=cityselect)
quitbutton = tk.Button(mainwin,text="Quitter",command=mainwin.quit)

title.pack() # Gestionnaire de géométrie "pack"
citylist.pack()
info.pack()
validbutton.pack()
quitbutton.pack()

mainwin.mainloop() # On passe la main à la boucle
                   # d'évènement

#!/usr/bin/env python3

from string import ascii_letters

# Usual Pythonic code:
def encode_char(c,k):
    '''Encode le caractère c (majuscule ou minuscule) avec la clef k'''

    if c not in ascii_letters:
        return c

    start = ord('a') if c.islower() else \
            ord('A') if c.isupper() else \
            None
    # les trois lignes ci-dessus sont équivalentes à
    # if c.islower():
    #     start = ord('a')
    # elif c.isupper():
    #     start = ord('A')
    # else:
    #     start = None # normalement impossible
    return chr( ( ord(c) - start + k ) % 26 + start )

def encode_string(msg,k):
    return ''.join( [ encode_char(c,k) for c in msg ] )

def decode_string(msg,k):
    return encode_string(msg, -k)

def brute_force(crypted, hint):
    for key in range(1,26):
        cleartext = decode_string(crypted,key).lower()
        if hint.lower() in cleartext:
            break
    else:
        return (None, None)  # not found :'-(
    return (cleartext, key)  # found :-)







# Test si on est un module (import ...ou from ... import)
# ou non (exécuté comme un programme)
if __name__ == '__main__':
    with open('crypted') as mystery:
        text = mystery.read().strip()
        clear, code = brute_force(text,'spam')
        if clear is not None:
            print("Found! the key was: {}".format(code))
            print(clear)
        else:
            print("Not found, call Obélix...")


#!/usr/bin/env python3
'''Génére la facture d'une commande de petit déjeuner'''

import configparser
import os.path

# où suis-je ?
mydir = os.path.dirname(__file__)

# Lecture du fichier de conf

config = configparser.ConfigParser()
config.read(f'{mydir}/config.ini')
config.sections()

datafile = f"{config['global']['datapath']}/prices.txt"

pricedb = {} # ou dict()
with open(datafile) as fd:
    for line in fd:
        food, price = line.rstrip().split()
        price = float(price) # Decimal serait mieux que float
        pricedb[food]= price

print('Bienvenue à Fawlty Towers!!!')
print('Menu :')
for food, price in pricedb.items():
    print(f"{food}\t{price}")

# TODO: redemander si aliment inconnu
order = input('\nVotre commande : ')
foods = order.split()

# TODO: Prendre en compte le nom de l'aliment le plus long
# pour aligner
print("**** Fawlty Towers ****")
total = 0
for food in foods:
    price = pricedb.get(food)
    if price is None:
        print('WARN: aliment inconnu :', food)
    else:
        total += price
        print(f"{food}\t£{price:.2f}")
print(f' Total : £{total:.2f}')


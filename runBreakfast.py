#!/usr/bin/env python3
'''Script de test du module proBreakfast'''

from proBreakfast import product_price, breakfast_price

food = input('Test avec un aliment : ').lower()

print("Prix :", product_price(food))

commande = input('Commande : ').lower().split()

print("Prix :", breakfast_price(commande))

# Print et listes

myFood = [ 'spam', 'egg', 'sausage']

# On peut "déballer" une séquence dans des
# arguments passé à une fonction
print('Print sur une liste :')
print(myFood)
print('Print sur une liste étoilée')
print(*myFood)

# Et si on étoile la définition de la fonction ?
# Voilà comment définir une fonction qui accepte des
# arguments par position en nombre quelconques
# comme print et bien d'autres
def breakfast_price_new(*foods):
    total = 0
    for food in foods:
        price = product_price(food)
        total += price if price is not None else 0
    return total

print('Fonction def breakfast_price_new(*foods) :')
print(breakfast_price_new('spam','egg','sausage'))

#!/usr/bin/env python3
'''Passe commande du petit déjeuner'''

order = input('Commande : ')

if 'spam' in order:
    order += ' good!'
elif 'ham' in order:
    order += ' spam'
else:
    order += ' spam spam'

if 'coffee' in order:
    order += ' spam' * 10

nspam = order.count('spam')

print('Assiette :', order)
print('Spam     :', nspam)


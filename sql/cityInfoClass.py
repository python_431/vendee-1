#!/usr/bin/env python3

class City:
    def __init__(self, name, pop=None, area=None, gent=None):
        self.name = name
        self.pop = pop
        self.area = area
        self.gent = gent

    def getInfo(self):
        return (self.name, self.pop, self.area, self.gent)

    def showInfo(self):
        print('{} {} {}'.format('***', self.name, '***'))
        if self.pop is not None:
            print('\t{} : {}'.format('population', self.pop))
        if self.area is not None:
            print('\t{} : {}'.format('   surface', self.area))
        if self.gent is not None:
            print('\t{} : {}'.format('   gentilé', self.gent))

    def __str__(self):
        return '{} : pop = {} area = {}'.format(self.name, self.pop, self.area)


if __name__ == '__main__':
    paris = City('Paris', 2206488, 105.4)
    edimbourg = City('Edinburgh', 513210, 264)
    londres = City('London', 8825000, 1572)
    morlaix = City('Morlaix')

    print('Test de getInfo')
    print(paris.getInfo())
    print(edimbourg.getInfo())
    print(City.getInfo(paris))  # c'est bien la même chose que la ligne précédente
    print(morlaix.getInfo())
    print('Accès direct aux attributs')
    print(paris.name)  # on peut accéder aux attributs de l'extérieur

    # avant l'ajout de la méthode __str__
    # on voyait qq chose du genre : <__main__.City object at 0x10bff0dd8>
    print('Test de print directement sur un objet :')
    print(paris)

    myCities = [paris, morlaix, edimbourg, londres]

    print('\nTest dans des boucles de getInfo et showInfo')
    for city in myCities:
        print(city.getInfo())

    for city in myCities:
        city.showInfo()

    print('\nTest de filtrage en compréhension')
    print("Villes de plus de 600000 habitants :")
    print([city.name for city in myCities if city.pop is not None and city.pop > 600000])
    print(*[city for city in myCities if city.pop is not None and city.pop > 600000], sep='\n')


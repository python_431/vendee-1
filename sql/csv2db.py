#!/usr/bin/env python3

import csv,sqlite3
import sys,os.path

dbfile  = 'cities.sqlite3'
csvfile = 'cities.csv'
if not os.path.isfile(dbfile):
    sys.stderr.write('{} introuvable\n'.format(dbfile))
    exit(1)


with open(csvfile) as f, \
     sqlite3.connect(dbfile) as c:
    rdr = csv.reader(f)
    curs = c.cursor()
    curs.execute('DELETE FROM cities')  # !!! attention on vide la table
    for name, pop, area, gent in rdr:
        pop = int(pop)
        area = float(area)
        curs.execute('INSERT INTO cities(name,pop,area,gent)' \
                     ' VALUES(?,?,?,?)',
                     (name, pop, area, gent))
        print('id: {} inserted'.format(curs.lastrowid))
    c.commit()



